(function() {
	angular.module('calcApp', [
		'ui.router',
		'ngAnimate',
		'ui.utils.masks',
	])
})()