(function() {
	angular.module('calcApp').config([
		'$stateProvider',
		'$urlRouterProvider',
		function($stateProvider, $urlRouterProvider) {
			$stateProvider.state('calculator', {
				url: "calculator",
				templateUrl: "calculator/calculator.html"
			})

			$urlRouterProvider.otherwise('/calculator')
		}
	])
})()