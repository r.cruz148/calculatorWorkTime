(function() {
	angular.module('calcApp').controller('CalculatorCtrl', [
		CalculatorController
	])

	function CalculatorController() {
		const vm = this
		const required = 8.4
		const tolerance = 10

		vm.$onInit = () => {
			vm.worked = toStringDate(required)
			vm.balance = "00:00"
			vm.begin = {value: "", label: "Entrada", placeholder: "00:00"}
			vm.lunchBegin = {value: "", label: "Início do Almoço", placeholder: "00:00"}
			vm.lunchEnd = {value: "", label: "Fim do Almoço", placeholder: "00:00"}
			vm.end = {value: "", label: "Sáida", placeholder: "00:00"}
		}

		function toDate(t) {

			var r = t.value.split(":")
			var d = new Date()
			d.setHours(r[0])
			d.setMinutes(r[1])
			d.setSeconds(0)
			d.setMilliseconds(0)

			return d
		}

		function formatTime(h, m) {
			var dhour = (h < 10 ? "0" : "")
			var dminute = (m < 10 ? "0" : "")
			return `${dhour}${h}:${dminute}${m}`
		}

		function toStringDate(d) {
			var hour = parseInt(d)
			var minute = parseInt(Math.round((d - hour)*60))
			return formatTime(hour, minute)
		}

		vm.calculateLunchEnd = function() {
			if (lunchBegin.value.length === 5) {
				var d = toDate(vm.lunchBegin)
				d.setHours((d.getHours() + 1))
				vm.lunchEnd.value = formatTime(d.getHours(), d.getMinutes())
				vm.end.value = __calculateEnd()
				vm.calculate()
			} else {
				vm.lunchEnd.value = ""
			}
		}

		function __calculateEnd() {
			var morningShift = toDate(vm.lunchBegin) - toDate(vm.begin)
			var restTime = ((required * 3600000) - morningShift)

			var d = toDate(vm.lunchEnd)
			d = new Date(d.getTime() + restTime)

			return formatTime(d.getHours(), d.getMinutes())
		}

		vm.calculateEnd = function() {
			if (lunchEnd.value.length === 5) {
				vm.end.value = __calculateEnd()
				vm.calculate()
			} else {
				vm.end.value = ""
			}
		}

		vm.calculate = function() {
			var w = ( toDate(vm.lunchBegin) - toDate(vm.begin) ) + ( toDate(vm.end) - toDate(vm.lunchEnd) )
			var worked = w / 3600000

			var b = worked - required
			var signal = ( b < 0 ? "-" : "")
			vm.balance = `${signal}${toStringDate(Math.abs(b))}`

			vm.worked = toStringDate(worked)

			vm.colorWorked = (worked >= required ? "bg-green" : "bg-gray")
			b = Math.round(b * 60)
			vm.colorBalance = ( b < -10 ? "bg-orange" : ( b > 10 ? "bg-green" : "bg-aqua"))

		}
	}
})()